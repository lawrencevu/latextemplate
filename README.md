Multiple-file LaTeX project template
====================================

This repository provides a template to start a LaTeX project for a long document where it would be better to split into multiple independent files to write.
The design is so that you (and your co-authors) can work on and TeX each section file `introduction.tex`, `results.tex` and `conclusion.tex` independently. When you are done, TeX the `main.tex` to create the full document with all sections included.
I also add the personal-note trick.
See [my website](https://lawrencevu.bitbucket.io/textips.html) for the explanation of how it works.
